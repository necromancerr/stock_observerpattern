package observer.pattern;

import org.joda.time.DateTime;

public class StockStatus implements IStockStatus {

    private StockDateTime stockDateTime;
    private StockMoney stockMoney;
    private BrokerNotificationStatusEnum brokerNotificationStatus;

    public StockStatus() {
        this.stockDateTime = new StockDateTime();
        this.stockMoney = new StockMoney(0.0);
        this.brokerNotificationStatus = BrokerNotificationStatusEnum.BROKERS_NOT_NOTIFIED;
    }
    
    public StockStatus(double value) {
    	this.stockDateTime = new StockDateTime();
    	this.stockMoney = new StockMoney(value);
    }

    public double getStockMoney() {
        return stockMoney.getValue();
    }

    public DateTime getStockDateTime() {
        return stockDateTime.getStockDateTime();
    }

    public BrokerNotificationStatusEnum getBrokerNotificationStatus() {
        return this.brokerNotificationStatus;
    }

    public void setBrokerNotificationStatusToNotified() {
        this.brokerNotificationStatus = BrokerNotificationStatusEnum.BROKERS_NOTIFIED_ONCE;
    }
}
