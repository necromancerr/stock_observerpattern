package observer.pattern;

import java.util.HashMap;

public class Stock extends Subject {

    private String stockSymbol;
    private IStockStatus stockStatus;


    public Stock(String aStockSymbol, IStockStatus stockStatus) {
        super();
        observersAndNotificationStatus = new HashMap<Observer, BrokerNotificationStatusEnum>();
        this.stockSymbol = aStockSymbol;
        this.stockStatus = stockStatus;
        notifyCreation(this);
    }

    public Stock getStock() {
        return this;
    }
    
    public String getStockSymbol() {
        return this.stockSymbol;
    }
    
    public void setStockStatus(IStockStatus newStockStatus) {
        System.out.println("Updating stock status for stock: " + this.getStockSymbol());
        this.stockStatus = newStockStatus;
        notifyPriceUpdate(this, newStockStatus);
        System.out.println("\n");
    }

    public IStockStatus getStockStatus() {
        return this.stockStatus;
    }
    
    @Override
    public void register(Observer observer, BrokerNotificationStatusEnum status) {
        super.register(observer, status);
        BrokerNotificationStatusEnum stockStatus = this.stockStatus.getBrokerNotificationStatus();
        if ( BrokerNotificationStatusEnum.BROKERS_NOT_NOTIFIED.equals(status) ||
            BrokerNotificationStatusEnum.BROKERS_NOT_NOTIFIED.equals(stockStatus)) {
            notifyCreation(observer, this);
            this.stockStatus.setBrokerNotificationStatusToNotified();
        }
    }

    @Override
    public void unregister(Observer observer) {
        super.unregister(observer);
        System.out.println("\nUnregistered " + observer.getClass().getSimpleName() + 
                " observer. It will not get updates for " + this.getStockSymbol() + " Stock anymore.");
    }
}
