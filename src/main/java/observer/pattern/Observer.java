package observer.pattern;

public interface Observer {
    void update(Stock stock);
    void update(Stock stock, IStockStatus stockStatus);
}
