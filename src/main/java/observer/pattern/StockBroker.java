package observer.pattern;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class StockBroker implements Observer{

    Map<String, Stock> stockDataStore;

    public StockBroker() {
        stockDataStore = new HashMap<String, Stock>();
    }

    public void update(Stock stock) {
        String stockSymbol = stock.getStockSymbol();
        stockDataStore.put(stockSymbol, stock);
        System.out.println(this.getClass().getSimpleName() +":" + this.hashCode() + 
                ": got the updated stock information for: " + stock.getStockSymbol());

    }

    public void update(Stock stock, IStockStatus stockStatus) {
    
    }

    public Stock getUpdatedStock(String stockSymbol) {
        Stock updatedStock = stockDataStore.get(stockSymbol);
        if (null == updatedStock) {
            throw new NoSuchElementException(String.format("Stock with symbol %s not found", stockSymbol));
        }
        return updatedStock;
    }

    public Collection<Stock> getStocks() {
        return stockDataStore.values();
    }

}