package observer.pattern;

import java.util.Map;

public abstract class Subject {

    Map<Observer, BrokerNotificationStatusEnum> observersAndNotificationStatus;

    public void register(Observer observer, BrokerNotificationStatusEnum status) {
        observersAndNotificationStatus.put(observer, status);
    }

    public void unregister(Observer observer) {
        observersAndNotificationStatus.remove(observer);
    }

    public void notifyCreation(Stock stock) {
        for (Observer observer : observersAndNotificationStatus.keySet()) {
            observer.update(stock);
        }
    }

    public void notifyCreation(Observer observer, Stock stock) {
        observer.update(stock);
    }

    public void notifyPriceUpdate(Stock stock, IStockStatus stockStatus) {
        for (Observer observer : observersAndNotificationStatus.keySet()) {
            observer.update(stock, stockStatus);
        }
    }
}