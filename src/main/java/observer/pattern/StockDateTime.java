package observer.pattern;

import org.joda.time.DateTime;

public class StockDateTime {

    DateTime dateTime;

    public StockDateTime() {
        this.dateTime = DateTime.now();
    }

    public DateTime getStockDateTime() {
        return dateTime;
    }

    public void updateToCurrentDateTime() {
        this.dateTime = DateTime.now();
    }
}
