package observer.pattern;

public enum BrokerNotificationStatusEnum {

    BROKERS_NOTIFIED_ONCE,
    BROKERS_NOT_NOTIFIED

}
