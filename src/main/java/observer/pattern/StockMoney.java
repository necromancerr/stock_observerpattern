package observer.pattern;

public class StockMoney {

    private double moneyValue = 0.0;
    
    public StockMoney(Double money) {
        this.moneyValue = money;
    }

    public double getValue() {
        return moneyValue;
    }

    public void incrementValue(Double incrementPrice) {
        this.moneyValue = this.moneyValue + incrementPrice;
    }

    public void decrementValue(Double decrementPrice) {
        this.moneyValue = this.moneyValue - decrementPrice;
        if (this.moneyValue < 0) {
            this.moneyValue = 0;
        }
    }
}
