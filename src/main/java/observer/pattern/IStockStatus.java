package observer.pattern;

import org.joda.time.DateTime;

public interface IStockStatus {

    double getStockMoney();
    DateTime getStockDateTime();
    BrokerNotificationStatusEnum getBrokerNotificationStatus();
    void setBrokerNotificationStatusToNotified();

}
