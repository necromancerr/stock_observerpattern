package observer.pattern;

import java.util.HashMap;
import java.util.Map;

public class StockMonitor implements Observer{

    private Map<String, Stock> stockMonitorDataStore;

    public StockMonitor() {
        stockMonitorDataStore = new HashMap<String, Stock>();
    }

    public void update(Stock stock) {
        stockMonitorDataStore.put(stock.getStockSymbol(), stock);
        System.out.println("New Stock: " + stock.getStockSymbol() + " is created and added to Stock Monitor");
    }

    public void update(Stock stock, IStockStatus stockStatus) {
        stockMonitorDataStore.put(stock.getStockSymbol(), stock);
        System.out.println("Stock Symbol displayed by Stock Monitor: " + 
                stock.getStockSymbol() + " updated. New price: " + stockStatus.getStockMoney());
    }

}
