package observer.pattern;

import java.util.HashSet;
import java.util.Set;

public class StockTester {

    private static Set<Observer> allObservers = new HashSet<Observer>();

    public static void main(String[] args) {

        StockMonitor stockMonitor = new StockMonitor();
        allObservers.add(stockMonitor);

        Set<StockBroker> stockBrokers = new HashSet<StockBroker>();
        StockBroker broker1 = new StockBroker();
        stockBrokers.add(broker1);

        StockBroker broker2 = new StockBroker();
        stockBrokers.add(broker2);
        System.out.println("StockBrokers created\n");

        allObservers.addAll(stockBrokers);

        System.out.println("\n\nCreating Stocked called APPL");
        Stock appleStock = new Stock("APPL", new StockStatus());
        registerAllObserversToStock(appleStock);

        System.out.println("\n\nCreating Stocked called GOOG");
        Stock googleStock = new Stock("GOOG", new StockStatus());
        registerAllObserversToStock(googleStock);

        System.out.println("\n\nCreating Stocked called SAMG");
        Stock samsungStock = new Stock("SAMG", new StockStatus());
        registerAllObserversToStock(samsungStock);

        appleStock.setStockStatus(new StockStatus(200.0));
        googleStock.setStockStatus(new StockStatus(500.0));
        appleStock.setStockStatus(new StockStatus(399.9));
        googleStock.setStockStatus(new StockStatus(900.0));
        samsungStock.setStockStatus(new StockStatus(299.9));
        appleStock.setStockStatus(new StockStatus(599.0));
        googleStock.setStockStatus(new StockStatus(799.9));

        samsungStock.setStockStatus(new StockStatus(199.9));

        samsungStock.unregister(stockMonitor);
        samsungStock.unregister(broker1);
        System.out.println("Updating samsung stock. You shouldn't see any update from monitor.");
        samsungStock.setStockStatus(new StockStatus(999.9));
        System.out.println("Updating samsung stock complete. If you didn't see any update from StockMonitor it is because it didn't get any update.");
        registerAllObserversToStock(samsungStock);
        samsungStock.setStockStatus(new StockStatus(23.23));
    }

    private static void registerAllObserversToStock(Stock stock) {
        for (Observer observer : allObservers) {
            stock.register(observer, BrokerNotificationStatusEnum.BROKERS_NOT_NOTIFIED);
        }
    }
}
